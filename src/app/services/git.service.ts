import { Injectable } from '@angular/core'
import { request } from '@octokit/request'
import { Observable } from 'rxjs'
import { ResponseData } from '../model/repository'

@Injectable({
  providedIn: 'root'
})
export class GitService {

  search(input: string, page = 1, stars: string, size: string): Observable<ResponseData> {
    return new Observable<ResponseData>(subscriber => {
      let query = input
      if (stars) {
        query += ` stars:>=${stars}`
      }
      if (size) {
        query += ` size:>=${size}`
      }
      request('GET /search/repositories', {
        q: query,
        per_page: 10,
        page,
      })
        .then(response => subscriber.next(response.data))
        .catch(error => subscriber.error(error))
        .finally(() => subscriber.complete())
    })
  }
}
