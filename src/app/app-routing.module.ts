import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SearchPageComponent } from './pages/search-page/search-page.component';
import { RepositoryPageComponent } from './pages/repository-page/repository-page.component';

const routes: Routes = [
  { path: ':repository-name', component: RepositoryPageComponent },
  { path: '**', redirectTo: "", component: SearchPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
