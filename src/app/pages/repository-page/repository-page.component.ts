import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { request } from '@octokit/request'
import { Repository } from 'src/app/model/repository'

@Component({
  selector: 'app-repository-page',
  templateUrl: './repository-page.component.html',
  styleUrls: ['./repository-page.component.scss']
})
export class RepositoryPageComponent implements OnInit {
  repository: Repository | null = null
  isLoading = true
  error: string | null = null

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    const repositoryFullName = this.route.snapshot.params['repository-name']
    request(`GET /repos/${repositoryFullName}`)
      .then(response => this.repository = response.data)
      .catch(error => this.error = error.message)
      .finally(() => this.isLoading = false)
  }

}
