import { Component, OnDestroy, OnInit } from '@angular/core'
import { Store } from '@ngrx/store'
import { FormControl } from '@angular/forms'
import { Observable, interval, BehaviorSubject, Subject } from 'rxjs'
import { map, debounce, filter, takeUntil, switchMap } from 'rxjs/operators'
import { PageEvent } from '@angular/material/paginator'

import { updateRequest } from 'src/app/store/repositories.actions'
import { RepositoryReducerState } from 'src/app/store/repositories.reducer'
import { Repository } from 'src/app/model/repository'

@Component({
	selector: 'app-search-page',
	templateUrl: './search-page.component.html',
	styleUrls: ['./search-page.component.scss']
})
export class SearchPageComponent implements OnInit, OnDestroy {
	searchInput = new FormControl('')
	minStarsInput = new FormControl('')
	sizeInput = new FormControl('')
	repositories$: Observable<Repository[]>
	isLoading$: Observable<boolean>
	totalCount$: Observable<number>
	error$: Observable<string | null>
	pageSubject$ = new BehaviorSubject<number>(1)
	minStarsSubject$ = new BehaviorSubject<string>('')
	sizeSubject$ = new BehaviorSubject<string>('')
	destroySubject$ = new Subject<void>()

	sizeOptions: { value: string; label: string }[] = [
		{ value: '', label: 'Не имеет значения' },
		{ value: '1000', label: '1 Мб' },
		{ value: '25000', label: '25 Мб' },
		{ value: '50000', label: '50 Мб' }
	];

	constructor(private store: Store<{ repositories: RepositoryReducerState }>) {
		this.repositories$ = store.select('repositories').pipe(map(({ data }) => data))
		this.isLoading$ = store.select('repositories').pipe(map(({ isLoading }) => isLoading))
		this.totalCount$ = store.select('repositories').pipe(map(({ total_count }) => total_count))
		this.error$ = store.select('repositories').pipe(map(({ error }) => error))
	}

	ngOnInit() {
		this.sizeInput.valueChanges.subscribe(value => this.sizeSubject$.next(value))

		this.minStarsInput.valueChanges.pipe(
			debounce(() => interval(500)),
			filter(value => !isNaN(parseInt(value, 10)) || value === ''),
			map(value => value || null),
			takeUntil(this.destroySubject$)
		).subscribe(value => this.minStarsSubject$.next(value))

		this.searchInput.valueChanges.pipe(
			debounce(() => interval(500)),
			filter(value => value),
			map(input => ({ input })),
			switchMap(searchQuery => this.pageSubject$.pipe(map(page => ({ ...searchQuery, page })))),
			switchMap(searchQuery => this.minStarsSubject$.pipe(map(stars => ({ ...searchQuery, stars })))),
			switchMap(searchQuery => this.sizeSubject$.pipe(map(size => ({ ...searchQuery, size })))),
			takeUntil(this.destroySubject$)
		)
			.subscribe((searchQuery) => this.store.dispatch(updateRequest(searchQuery)))
	}

	onPageChange(event: PageEvent) {
		this.pageSubject$.next(event.pageIndex + 1)
	}

	ngOnDestroy() {
		this.destroySubject$.next()
	}
}
