import { Component, Input, OnInit } from '@angular/core'
import { Repository } from 'src/app/model/repository'

@Component({
  selector: 'app-repository-card',
  templateUrl: './repository-card.component.html',
  styleUrls: ['./repository-card.component.scss']
})
export class RepositoryCardComponent implements OnInit {
  @Input() repository: Repository | null = null

  constructor() { }

  ngOnInit(): void {
  }

}
