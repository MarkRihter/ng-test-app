import { createAction, props } from '@ngrx/store'
import { Repository } from '../model/repository'

export const updateRequest = createAction('[Search Component] Update Request', props<{ input: string, page?: number, stars: string, size: string }>())
export const updateResponse = createAction('[Search Component] Update Response', props<{ items: Repository[], total_count: number }>())
export const updateError = createAction('[Search Component] Update Error', props<{ error: string }>())