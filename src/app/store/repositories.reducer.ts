import { createReducer, on, Action, State } from '@ngrx/store'
import { updateResponse, updateRequest, updateError } from './repositories.actions'
import { Repository } from '../model/repository';

export interface RepositoryReducerState {
    data: Repository[];
    isLoading: boolean;
    total_count: number;
    error: string | null;
}

export const initialState: RepositoryReducerState = {
    data: [],
    isLoading: false,
    total_count: 0,
    error: null,
};

const _repositoriesReducer = createReducer(
    initialState,
    on(updateRequest, (state) => ({ ...state, data: [], isLoading: true, error: null })),
    on(updateResponse, (state, action) => ({ ...state, data: action.items, total_count: action.total_count, isLoading: false })),
    on(updateError, (state, action) => ({ ...state, error: action.error, isLoading: false }))
);

export function repositoriesReducer(state: any, action: Action) {
    return _repositoriesReducer(state, action)
}