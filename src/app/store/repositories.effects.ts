import { Injectable } from '@angular/core'
import { Actions, createEffect, ofType } from '@ngrx/effects'
import { of } from 'rxjs'
import { map, mergeMap, catchError } from 'rxjs/operators'
import { GitService } from '../services/git.service'
import { updateRequest, updateResponse, updateError } from './repositories.actions'

@Injectable()
export class RepositoriesEffects {

    constructor(
        private actions$: Actions,
        private gitService: GitService,
    ) { }

    updateRepositories = createEffect(() => this.actions$.pipe(
        ofType(updateRequest),
        mergeMap(({ input, page, stars, size }) => this.gitService.search(input, page, stars, size).pipe(
            map(({ items, total_count }) => updateResponse({ items, total_count })),
            catchError((error) => of(updateError({ error: error.message }))
            )
        ))
    ))
}