import { Endpoints } from "@octokit/types"

export type ResponseData = Endpoints['GET /search/repositories']['response']['data']
export type Repository = ResponseData['items'][0]