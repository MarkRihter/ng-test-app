import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { StoreModule } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'
import { SearchPageComponent } from './pages/search-page/search-page.component'
import { RepositoryPageComponent } from './pages/repository-page/repository-page.component'
import { ReactiveFormsModule } from '@angular/forms'

import { MatInputModule } from '@angular/material/input'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatIconModule } from '@angular/material/icon'
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'
import { MatPaginatorModule } from '@angular/material/paginator'
import { MatCardModule } from '@angular/material/card'
import { MatSelectModule } from '@angular/material/select'

import { repositoriesReducer } from './store/repositories.reducer'
import { RepositoriesEffects } from './store/repositories.effects'
import { RepositoryCardComponent } from './pages/search-page/repository-card/repository-card.component'

@NgModule({
  declarations: [
    AppComponent,
    SearchPageComponent,
    RepositoryPageComponent,
    RepositoryCardComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    StoreModule.forRoot({ repositories: repositoriesReducer }, {}),
    EffectsModule.forRoot([RepositoriesEffects]),
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatCardModule,
    MatSelectModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
